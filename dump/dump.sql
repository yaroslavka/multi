-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 22 2016 г., 00:25
-- Версия сервера: 5.5.52-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `moscall`
--

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_descr_ru` text NOT NULL,
  `form_descr_en` text NOT NULL,
  `top_title_ru` varchar(255) NOT NULL,
  `top_title_en` varchar(255) NOT NULL,
  `top_descr_ru` text NOT NULL,
  `top_descr_en` text NOT NULL,
  `bottom_title_ru` varchar(255) NOT NULL,
  `bottom_title_en` text NOT NULL,
  `bottom_descr_ru` text NOT NULL,
  `bottom_descr_en` text NOT NULL,
  `rate_descr_ru` text NOT NULL,
  `rate_descr_en` text NOT NULL,
  `link_vk` varchar(255) NOT NULL,
  `link_fb` varchar(255) NOT NULL,
  `link_android` varchar(255) NOT NULL,
  `link_ios` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='настройки' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `form_descr_ru`, `form_descr_en`, `top_title_ru`, `top_title_en`, `top_descr_ru`, `top_descr_en`, `bottom_title_ru`, `bottom_title_en`, `bottom_descr_ru`, `bottom_descr_en`, `rate_descr_ru`, `rate_descr_en`, `link_vk`, `link_fb`, `link_android`, `link_ios`, `image`) VALUES
(1, 'У вас есть вопрос? Задайте его и мы в кратчайшие сроки ответим на вопрос.', 'Do you have a question? Ask him, and we will promptly reply to a question.', 'ВЫГОДНЫЕ ЗВОНКИ НА РОДИНУ', 'GREAT CALLS TO HOMELAND', 'Благодаря применению новейших технологий в области коммуникаций, наш сервис обеспечит надежную связь с вашими близкими. Благодаря качественной связи и выгодным тарифам вы никогда не потеряете связь	с родными.', 'Through the use of latest technologies in the field of communications, our service will ensure reliable communication with your loved ones. Through good communication and great rates you''ll never lose touch with their relatives.', 'La présentation de la page principale, est toujours sur l''approbation du', '1123123123123', 'Moscall - приложение, которое позволяет всегда оставаться на связи. Скачайте и установите приложение, чтобы осуществлять дешевые звонки с качественной связью.', 'Moscall - an application that allows you to stay connected. Download and install the application to carry out cheap calls from high-quality communications.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut \r\nlaboreet dolore magna aliqua. Ut enim ad minim veniam,', '1Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut \r\nlaboreet dolore magna aliqua. Ut enim ad minim veniam,', 'vk.com', 'fb.com', 'wwww.com', 'wwww.com', 'c763a05775a1709062b4ea1c7d0ca2ac.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;