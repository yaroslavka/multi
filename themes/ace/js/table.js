(function($) {
    $('body').on('click','.delete_all',function(){
         var mass=[];
            $('input[name="delete-box"]:checked').each(function(){
                mass.push($(this).val());
            })
            if(mass.length>0){
                if (confirm('Продолжить удаление?')) {
                $.ajax({
                    url: $(this).attr('data-href'),
                    type: 'post',
                    dataType: 'json',
                    data:{mass:mass},
                    success: function(r) {
                       window.location.href = window.location.href;
                    }
                });
               }
            }else{
               alert('Выберите акции'); 
            }
            return false;
    });
    block =1;
    $('body').on('click', '#add-discount', function() {
        var id = block++;
        var fields = $('.new-block').html();
        var cl = $(this).attr('data-class');
        $('.more-discount').prepend('<div class="discount-block-new-' + id + '">' + fields + '<input type="button" value="удалить" data-id="' + id + '" class="del-discount-new"></div>');
        $('.discount-block-new-' + id).find('#discount').attr('id', 'new-' + id).attr('name', cl + '[discount_array]["new' + id + '"][discount]');
        $('.discount-block-new-' + id).find('#min_man').attr('id', 'new-min_man' + id).attr('name', cl + '[discount_array]["new' + id + '"][koll]');
    });
    
    $('body').on('click', '.del-discount-new', function() {
        $('.discount-block-new-' + $(this).attr('data-id')).remove();
    });
    
     $('body').on('click', '.del-discount', function() {
        $('.discount_array-block-' + $(this).attr('data-id')).remove();
    });
    
})(jQuery);