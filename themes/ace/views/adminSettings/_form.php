<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
));
?>
<?php if ($model->errors): ?>
    <div class="well">
        <?php echo $form->errorSummary($model); ?>
    </div>
<?php endif; ?>
<div class="control-group">
    <?php echo $form->labelEx($model, 'top_title_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'top_title_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'top_title_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'top_title_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'top_descr_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'top_descr_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'top_descr_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'top_descr_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'bottom_descr_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'bottom_descr_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'bottom_descr_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'bottom_descr_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'form_descr_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'form_descr_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'form_descr_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'form_descr_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'rate_descr_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'rate_descr_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'rate_descr_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'rate_descr_en', array('class' => 'span4')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'link_vk', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'link_vk', array('class' => 'span4')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'link_fb', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'link_fb', array('class' => 'span4')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'link_android', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'link_android', array('class' => 'span4')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'link_ios', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'link_ios', array('class' => 'span4')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'image', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php if (!empty($model->image)): ?>
            <?php if (file_exists(Yii::getPathOfAlias('webroot') . '/' . $model->getImagePath('logo'))): ?>
                <div id="file_image">
                    <?php echo CHtml::image(Yii::app()->baseUrl.$model->getImagePath('logo'));?>
                    <?php echo CHtml::ajaxLink('Удалить', array('fileDelete', 'id' => $model->primaryKey, 'field' => 'image'), array('success' => 'js:function(){$("#file_image").remove();}'), array('data-dismiss' => "fileupload", 'class' => 'btn btn-delete btn-small')); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php echo $form->fileField($model, 'file_image', array('id' => "id-input-file-2",'class' => 'span4')); ?>
    </div>
</div>

<div class="form-actions">
    <?php echo CHtml::submitButton("Сохранить", array("class" => "btn btn-info")); ?>
</div>
<?php $this->endWidget(); ?>
