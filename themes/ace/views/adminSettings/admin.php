<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/table.js',CClientScript::POS_END);?>
<div class="page-header position-relative">
    <h1>Настройки Сайта</h1>
</div>
<div class="row-fluid">
    <div class="span12">
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>
