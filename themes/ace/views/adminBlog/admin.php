<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/table.js', CClientScript::POS_END); ?>
<div class="page-header position-relative">
    <h1>Таблица</h1>
</div>
<div class="row-fluid">
    <ul class="nav nav-tabs">
        <li class="<?php echo Yii::app()->controller->action->id == 'admin'?'active':'';?>"><?php echo CHtml::link('Таблица', array('admin')); ?></li>
        <li class="<?php echo Yii::app()->controller->action->id == 'create'?'active':'';?>"><?php echo CHtml::link('Создать', array('create')); ?></li>
    </ul>
</div>
<div class="row-fluid">
    <div class="span12">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'admin-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'itemsCssClass' => 'table table-striped table-bordered table-hover',
            'columns' => array(
                'id',
                'title_ru',
                array(
                    'class' => 'MyCButtonColumn',
                    'template' => '{view}{update}{delete}',
                    'buttons' => array(
                        'view' => array(
                            'label' => '<i class="icon-ok bigger-120"></i>',
                            'flag' => true,
                            'options' => array("class" => "btn btn-mini btn-info"),
                            'imageUrl' => false,
                            'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/view", array("id"=>$data->id))',
                        ),
                        'update' => array(
                            'label' => '<i class="icon-edit bigger-120"></i>',
                            'flag' => true,
                            'options' => array("class" => "btn btn-mini btn-success"),
                            'imageUrl' => false,
                            'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/update", array("id"=>$data->id))',
                        ),
                        'delete' => array(
                            'label' => '<i class="icon-trash bigger-120"></i>',
                            'flag' => true,
                            'options' => array("class" => "btn btn-mini btn-danger", 'rel' => 'bootbox-confirm'),
                            'imageUrl' => false,
                            'url' => 'Yii::app()->createUrl(Yii::app()->controller->id."/delete", array("id"=>$data->id))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
