<div class="page-header position-relative">
    <h1>Создать</h1>
</div>
<div class="row-fluid">
    <ul class="nav nav-tabs">
        <li><?php echo CHtml::link('Таблица', array('admin')); ?></li>
    </ul>
</div>
<hr>
<div class="row-fluid">
    <div class="span12">
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>
