<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
));
?>
<?php if ($model->errors): ?>
    <div class="well">
        <?php echo $form->errorSummary($model); ?>
    </div>
<?php endif; ?>
<div class="control-group">
    <?php echo $form->labelEx($model, 'login', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'login', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'hashpassword', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->passwordField($model, 'hashpassword', array('class' => 'span4')); ?>
    </div>
</div>
<div class="form-actions">
    <?php echo CHtml::submitButton("Сохранить", array("class" => "btn btn-info")); ?>
</div>
<?php $this->endWidget(); ?>
