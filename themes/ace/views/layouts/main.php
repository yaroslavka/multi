<?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Управление</title>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/css/main.css'); ?>
    <?php Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Roboto:400,100,300,900,700,500,100italic&subset=latin,cyrillic-ext'); ?>
</head>
<body>
<?php $this->renderPartial('webroot.themes.ace.views.layouts.navbar'); ?>

<div class="main-container container-fluid">
    <a class="menu-toggler" id="menu-toggler" href="#">
        <span class="menu-text"></span>
    </a>

    <div class="sidebar" id="sidebar">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                <button class="btn btn-small btn-success">
                    <i class="icon-signal"></i>
                </button>

                <button class="btn btn-small btn-info">
                    <i class="icon-pencil"></i>
                </button>

                <button class="btn btn-small btn-warning">
                    <i class="icon-group"></i>
                </button>

                <button class="btn btn-small btn-danger">
                    <i class="icon-cogs"></i>
                </button>
            </div>

            <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                <span class="btn btn-success"></span>

                <span class="btn btn-info"></span>

                <span class="btn btn-warning"></span>

                <span class="btn btn-danger"></span>
            </div>
        </div>

        <?php $this->renderPartial('webroot.themes.ace.views.layouts.menu'); ?>

        <div class="sidebar-collapse" id="sidebar-collapse">
            <i class="icon-double-angle-left"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="<?php echo Yii::app()->baseUrl; ?>">Главная</a>

                                        <span class="divider">
                                                <i class="icon-angle-right arrow-icon"></i>
                                        </span>
                </li>
            </ul>
        </div>

        <div class="page-content">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php if ($this->beginCache('_main_ace2', array('duration' => 3600))) { ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>
    <?php $this->endCache();
} ?>
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/js/main.js', CClientScript::POS_BEGIN); ?>
<script type="text/javascript">
    $(function () {
        $('[data-rel=tooltip]').tooltip();
        $('[data-rel=popover]').popover({html: true});

        $("[rel='bootbox-confirm']").on('click', function () {
            var location = $(this).attr('href');
            bootbox.confirm("Вы Уверены?", function (confirmed) {
                if (confirmed) {
                    window.location.replace(location);
                }
            });

            return false;
        });
    });
</script>
</body>
</html>
