<?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Редактирование и создание</title>
    <meta name="description" content="Static &amp; Dynamic Tables"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/css/main.css'); ?>
    <script src="http://api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>

</head>
<body>
<?php $this->renderPartial('webroot.themes.ace.views.layouts.navbar'); ?>
<div class="main-container container-fluid">
    <a class="menu-toggler" id="menu-toggler" href="#">
        <span class="menu-text"></span>
    </a>

    <div class="sidebar" id="sidebar">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                <button class="btn btn-small btn-success">
                    <i class="icon-signal"></i>
                </button>

                <button class="btn btn-small btn-info">
                    <i class="icon-pencil"></i>
                </button>

                <button class="btn btn-small btn-warning">
                    <i class="icon-group"></i>
                </button>

                <button class="btn btn-small btn-danger">
                    <i class="icon-cogs"></i>
                </button>
            </div>

            <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                <span class="btn btn-success"></span>

                <span class="btn btn-info"></span>

                <span class="btn btn-warning"></span>

                <span class="btn btn-danger"></span>
            </div>
        </div>

        <?php $this->renderPartial('webroot.themes.ace.views.layouts.menu'); ?>

        <div class="sidebar-collapse" id="sidebar-collapse">
            <i class="icon-double-angle-left"></i>
        </div>
    </div>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="<?php echo Yii::app()->baseUrl; ?>">Главная</a>

                                        <span class="divider">
                                                <i class="icon-angle-right arrow-icon"></i>
                                        </span>
                </li>
            </ul>
        </div>

        <div class="page-content">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php if ($this->beginCache('_main_ace2', array('duration' => 3600))) { ?>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>
    <?php $this->endCache();
} ?>
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/js/main_create.js', CClientScript::POS_BEGIN); ?>
<script type="text/javascript">
    $(function () {
        $('[data-rel=tooltip]').tooltip();
        $('[data-rel=popover]').popover({html: true});
        $('.input-mask-eyescript').mask('9-999-99-99-99');
        $('.input-mask-eyescript1').mask('99:99-99:99');

        $('#id-input-file-f').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|xls|xlsx|docs|pdf|pdfx|jpg|jpeg|txt',
            blacklist: 'exe|php'
        });
        $('#id-input-file-2').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'gif|png|jpg|jpeg',
            blacklist: 'exe|php'
        });
        $('#id-input-file-3').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|pdf|xls|xlsx|png|jpg|txt',
            blacklist: 'exe|php'
        });
        $('#id-input-file-4').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|pdf|xls|xlsx|png|jpg|txt',
            blacklist: 'exe|php'
        });
        $('#id-input-file-5').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|pdf|xls|xlsx|png|jpg|txt',
            blacklist: 'exe|php'
        });
        $('#id-input-file-6').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|pdf|xls|xlsx|png|jpg|txt',
            blacklist: 'exe|php'
        });
        $('#id-input-file-7').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|pdf|xls|xlsx|png|jpg|txt',
            blacklist: 'exe|php'
        });
        $('#id-input-file-8').ace_file_input({
            no_file: 'Файл не выбран...',
            btn_choose: 'Выбрать',
            btn_change: 'Изменить',
            droppable: false,
            onchange: null,
            thumbnail: 'large',
            whitelist: 'doc|pdf|xls|xlsx|png|jpg|txt',
            blacklist: 'exe|php'
        });
    });
</script>
</body>
</html>
