<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a href="<?php echo Yii::app()->createUrl('/site/index');?>" class="brand">
                <small>
                    <i class="icon-leaf"></i>
                    <?php echo Yii::app()->name;?> Админка
                </small>
            </a>
            <ul class="nav ace-nav pull-right">
                <?php if(Yii::app()->user->checkAccess('Moderator')):?>
                    <li class="grey">
                        <a href="<?php echo Yii::app()->createUrl('/adminAbout');?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </li>
                    <li class="orange">
                        <a href="<?php echo Yii::app()->createUrl('/adminClients');?>">
                            <i class="icon-group"></i>
                        </a>
                    </li>
                    <li class="green">
                        <a href="<?php echo Yii::app()->createUrl('/adminReview');?>">
                            <i class="icon-bell-alt"></i>
                        </a>
                    </li>
                <?php endif;?>
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo"
                             src="<?php echo Yii::app()->theme->baseUrl;?>/assets/avatars/user.jpg"
                             alt="Jason's Photo"/>
                        <span class="user-info">
                            <small>Здравствуйте</small>
                        </span>
                        <i class="icon-caret-down"></i>
                    </a>
                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                        <?php if(Yii::app()->user->checkAccess('Moderator')):?>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/adminSettings');?>">
                                    <i class="icon-cog"></i>
                                    Настройки
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/adminDelivery');?>">
                                    <i class="icon-list"></i>
                                    Доставка
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/adminContact');?>">
                                    <i class="icon-list"></i>
                                    Контакты
                                </a>
                            </li>
                        <?php endif;?>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('/site/logout');?>">
                                <i class="icon-off"></i>
                                Выход
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>