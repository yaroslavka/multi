<ul class="nav nav-list">
    <li<?php echo strstr($_SERVER['REQUEST_URI'], 'adminUser') ? ' class="active"' : ''; ?>>
        <?php echo CHtml::link('<i class="icon-user"></i><span class="menu-text">Пользователи<span class="badge badge-primary"></span></span>', array('/adminUser')); ?>
    </li>
    <li<?php echo strstr($_SERVER['REQUEST_URI'], 'adminAdvantage') ? ' class="active"' : ''; ?>>
        <?php echo CHtml::link('<i class="icon-star"></i><span class="menu-text">Преймущества<span class="badge badge-primary"></span></span>', array('/adminAdvantage')); ?>
    </li>
    <li<?php echo strstr($_SERVER['REQUEST_URI'], 'adminReview') ? ' class="active"' : ''; ?>>
        <?php echo CHtml::link('<i class="icon-signal"></i><span class="menu-text">Отзывы<span class="badge badge-primary"></span></span>', array('/adminReview')); ?>
    </li>
    <li<?php echo strstr($_SERVER['REQUEST_URI'], 'adminContacts') ? ' class="active"' : ''; ?>>
        <?php echo CHtml::link('<i class="icon-phone"></i><span class="menu-text">Контакты<span class="badge badge-primary"></span></span>', array('/adminContacts')); ?>
    </li>
    <li<?php echo strstr($_SERVER['REQUEST_URI'], 'adminSettings') ? ' class="active"' : ''; ?>>
        <?php echo CHtml::link('<i class="icon-wrench"></i><span class="menu-text">Настройки<span class="badge badge-primary"></span></span>', array('/adminSettings')); ?>
    </li>
    <li<?php echo strstr($_SERVER['REQUEST_URI'], 'adminStatics') ? ' class="active"' : ''; ?>>
        <?php echo CHtml::link('<i class="icon-wrench"></i><span class="menu-text">Статические стр.<span class="badge badge-primary"></span></span>', array('/adminStatics')); ?>
    </li>
</ul>
