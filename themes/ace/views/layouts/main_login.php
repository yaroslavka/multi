<?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Вход</title>
    <meta name="description" content="Static &amp; Dynamic Tables"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/assets/css/main.css'); ?>
    <link
        href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,900,700,500,100italic&subset=latin,cyrillic-ext'
        rel='stylesheet' type='text/css'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body class="login-layout">
<div class="main-container container-fluid">
    <div class="main-content">
        <div class="row-fluid">
            <div class="span12">
                <div class="login-container">
                    <div class="row-fluid">
                        <div class="center">
                            <h1>
                                <i class="icon-leaf green"></i>
                                <span class="red">API</span>
                                <span class="white">Application</span>
                            </h1>
                            <h4 class="blue">yaroslavka</h4>
                        </div>
                    </div>

                    <div class="space-6"></div>

                    <div class="row-fluid">
                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="icon-coffee green"></i>
                                            Введите Ваши данные
                                        </h4>

                                        <div class="space-6"></div>

                                        <?php echo $content; ?>
                                    </div>

                                    <div class="toolbar clearfix">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/js/bootstrap.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/js/ace-elements.min.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/js/ace.min.js'); ?>
</body>
</body>
</html>
