<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
));
?>
<?php if ($model->errors): ?>
    <div class="well">
        <?php echo $form->errorSummary($model); ?>
    </div>
<?php endif; ?>
<div class="control-group">
    <?php echo $form->labelEx($model, 'address_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'address_ru', array('class' => 'span4','col'=>10)); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'address_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'address_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'phone', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'skype', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'skype', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'email', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->emailField($model, 'email', array('class' => 'span4')); ?>
    </div>
</div>
<div class="form-actions">
    <?php echo CHtml::submitButton("Сохранить", array("class" => "btn btn-info")); ?>
</div>
<?php $this->endWidget(); ?>
