<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-form',
    'enableAjaxValidation' => false,
        ));
?>
<?php echo $form->errorSummary($model); ?>
<fieldset>
    <label>
        <span class="block input-icon input-icon-right">
            <?php echo $form->textField($model, 'username', array('class' => 'span12', 'placeholder' => 'логин')); ?>
            <i class="icon-user"></i>
        </span>
    </label>

    <label>
        <span class="block input-icon input-icon-right">
            <?php echo $form->passwordField($model, 'password', array('class' => 'span12', 'placeholder' => 'Пароль')); ?>
            <i class="icon-lock"></i>
        </span>
    </label>
    <div class="space"></div>
    <div class="clearfix">
        <button class="width-35 pull-right btn btn-small btn-primary" type="submit">
            <i class="icon-key"></i>
            Войти
        </button>
    </div>
    <div class="space-4"></div>
</fieldset>

<?php $this->endWidget(); ?>
