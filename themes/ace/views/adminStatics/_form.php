
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/table.js', CClientScript::POS_END); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'admin-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
        ));
?>
<div class="control-group">
    <?php echo $form->labelEx($model, 'title_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'title_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'title_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'title_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'title_fr', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'title_fr', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'body_ru', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'body_ru', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'body_en', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'body_en', array('class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'body_fr', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textArea($model, 'body_fr', array('class' => 'span4')); ?>
    </div>
</div>
<p class="before-extra-phone"></p>
<div class="control-group">
    <?php echo $form->labelEx($model, 'image', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php if (!empty($model->image)): ?>
            <?php if (file_exists(Yii::getPathOfAlias('webroot') . '/' . $model->getImagePath('logo'))): ?>
                <div id="file_image">
                    <?php echo CHtml::image(Yii::app()->baseUrl.$model->getImagePath('logo'));?>
                    <?php echo CHtml::ajaxLink('Удалить', array('fileDelete', 'id' => $model->primaryKey, 'field' => 'image'), array('success' => 'js:function(){$("#file_image").remove();}'), array('data-dismiss' => "fileupload", 'class' => 'btn btn-delete btn-small')); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php echo $form->fileField($model, 'file_image', array('id' => "id-input-file-2",'class' => 'span4')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'translit', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'translit', array('class' => 'span4')); ?>
    </div>
</div>
<div class="form-actions">
    <?php echo CHtml::submitButton("Сохранить", array("class" => "btn btn-info")); ?>
</div>
<?php $this->endWidget(); ?>