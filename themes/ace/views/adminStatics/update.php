<div class="page-header position-relative">
    <h1>Редактировать</h1>
</div>
<div class="row-fluid">
    <ul class="nav nav-tabs">
        <li><?php echo CHtml::link('Таблица', array('admin')); ?></li>
        <li class="active"><?php echo CHtml::link('Редактировать', array('update', 'id' => $model->primaryKey)); ?></li>
        <li><?php echo CHtml::link('Смотреть', array('view', 'id' => $model->primaryKey)); ?></li>
    </ul>
</div>
<hr>
<div class="row-fluid">
    <div class="span12">
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>
