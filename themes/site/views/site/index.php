<section class="main-section">
    <div class="container">
        <div class="col-md-3">
            <div class="main-image-cathedral  wow fadeIn"  data-wow-delay="0.5s">
                <img src="img/header-cathedral.png" alt="Cathedral">
            </div>
            <div class="mobile-header-img">
                <img src="img/header-mobile.png" alt="Mobile Phone">
            </div>
        </div>
        <div class="col-md-2">
            <div class="main-phone-image  wow fadeIn"  data-wow-delay="0.5s">
                <img src="img/moscall-gif2.gif" alt="Phone">
            </div>
        </div>
        <div class="col-lg-6 col-lg-offset-1 col-md-7">
            <div class="main-text-wrapper  wow fadeIn" data-wow-delay="0.5s">
                <h1><?php echo $settings->getAttributeByField('top_title_');?></h1>							
                <p><?php echo $settings->getAttributeByField('top_descr_');?></p>																		
            </div>
            <div class="mobile-buttons  wow fadeIn" data-wow-delay="0.5s">
                <?php if(!empty($settings->link_android)):?>
                    <a href="<?php echo $settings->link_android?>">
                        <img src="img/googlee-play-btn.png" alt="">
                        <img src="img/googlee-play-btn1.png" alt="">
                    </a>
                <?php endif;?>
                <?php if(!empty($settings->link_ios)):?>
                    <a href="<?php echo $settings->link_ios?>">
                        <img src="img/appstore-btn.png" alt="">
                        <img src="img/appstore-btn1.png" alt="">
                    </a>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($advantages)): ?>
<section class="advantages">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2><?php echo Yii::t('common', 'Преймущества'); ?> Moscall</h2>
                </div>
            </div>			
        </div>
        <div class="row">
            <?php foreach ($advantages as $key => $advantage): ?>
                <div class="col-md-4 col-sm-4  wow fadeInUp"  data-wow-delay="0.5s">
                    <div class="advantages-item">
                        <div class="advantages-img">
                            <img src="<?php echo $advantage->getImagePath('logo'); ?>" alt="Our Advantages">
                        </div>
                        <div class="advantages-text">
                            <h3><?php echo $advantage->title; ?></h3>
                            <p><?php echo $advantage->descr; ?></p>
                        </div>
                    </div>
                </div>
           <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="rates">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2><?php echo Yii::t('common', 'Тарифы'); ?></h2>
                    <p><?php echo $settings->getAttributeByField('rate_descr_');?></p>
                </div>
            </div>				
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="cloud-image">
                    <img src="img/cloud.png" alt="Cloud">
                </div>		
                <div class="rates-slider-wrapper wow fadeIn" data-wow-delay="0.9s">
                    <div class="rates-slider">
                        <h3 class="rates-title">Стандартный</h3>
                        <p class="rates-price">100, 99 <i class="fa fa-rub" aria-hidden="true"></i></p>
                        <p class="rates-text">Здесь идеть описание тарифа, 
                            стоимость звонков, кому 
                            подойдет тариф и остальная 
                            информация,тарификация и 
                            прочее</p>
                        <a href="#" class="buy-rate">Купить</a>					
                    </div>
                    <div class="rates-slider">
                        <h3 class="rates-title">Мега пакет</h3>
                        <p class="rates-price">100, 99 <i class="fa fa-rub" aria-hidden="true"></i></p>
                        <p class="rates-text">Здесь идеть описание тарифа, 
                            стоимость звонков, кому 
                            подойдет тариф и остальная 
                            информация,тарификация и 
                            прочее</p>
                        <a href="#" class="buy-rate">Купить</a>	
                    </div>
                    <div class="rates-slider">
                        <h3 class="rates-title">Премиум</h3>
                        <p class="rates-price">100, 99 <i class="fa fa-rub" aria-hidden="true"></i></p>
                        <p class="rates-text">Здесь идеть описание тарифа, 
                            стоимость звонков, кому 
                            подойдет тариф и остальная 
                            информация,тарификация и 
                            прочее</p>
                        <a href="#" class="buy-rate">Купить</a>		
                    </div>
                    <div class="rates-slider">
                        <h3 class="rates-title">Стандартный</h3>
                        <p class="rates-price">100, 99 <i class="fa fa-rub" aria-hidden="true"></i></p>							
                        <p class="rates-text">Здесь идеть описание тарифа, 
                            стоимость звонков, кому 
                            подойдет тариф и остальная 
                            информация,тарификация и 
                            прочее</p>
                        <a href="#" class="buy-rate">Купить</a>	
                    </div>
                    <div class="rates-slider">
                        <h3 class="rates-title">Стандартный</h3>
                        <p class="rates-price">100, 99 <i class="fa fa-rub" aria-hidden="true"></i></p>
                        <p class="rates-text">Здесь идеть описание тарифа, 
                            стоимость звонков, кому 
                            подойдет тариф и остальная 
                            информация,тарификация и 
                            прочее</p>
                        <a href="#" class="buy-rate">Купить</a>	
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="rates-image">
                <img src="img/kreml.png" alt="Kreml">
            </div>
        </div>
    </div>
</section>
<?php if(!empty($reviews)):?>
<section class="reviews" id="sn1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2><?php echo Yii::t('common', 'Отзывы Клиентов'); ?></h2>
                </div>
            </div>				
        </div>
        <div class="row">
            <div class="col-md-12">		
                <div class="reviews-slider-wrapper  wow fadeIn" data-wow-delay="0.5s">
                    <?php foreach ($reviews as $key => $review): ?>
                    <div class="reviews-slider">
                        <div class="reviews-image-wrapper">
                            <img src="<?php echo $review->getImagePath('mini'); ?>" alt="<?php echo $review->title; ?>">
                            <h3><?php echo $review->title; ?></h3>
                            <p><?php echo $review->subTitle; ?></p>
                        </div>
                        <div class="reviews-text-wrapper">
                            <p><?php echo $review->descr; ?></p> 
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>	
            </div>
        </div>
    </div>
</section>
<?php endif;?>
<section class="contacts-page" id="sn2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2><?php echo Yii::t('common', 'Контакты'); ?></h2>
                </div>
            </div>				
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="contacts-img-wrapper wow fadeInLeft"   data-wow-delay="0.7s">
                    <img src="img/contacts-phone.png" alt="Contacts Phone">
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div class="contacts-adress-wrapper  wow fadeIn"  data-wow-delay="0.5s">
                    <p>Email: <span><?php echo $contacts->email? $contacts->email :''; ?></span></p>
                    <p>Skype: <span><?php echo $contacts->skype? $contacts->skype :''; ?></span></p>
                    <p>Адрес: <span><?php echo $contacts->address? $contacts->address :''; ?></span></p>
                </div>
            </div>
            <div class="col-md-5">
                <form class="contacts-form-wrapper  wow fadeIn"  data-wow-delay="0.5s">
                    <h3><?php echo $settings->getAttributeByField('form_descr_');?></h3>
                    <input type="text" placeholder="<?php echo Yii::t('common', 'Имя'); ?>">
                    <input type="email" placeholder="<?php echo Yii::t('common', 'Email'); ?>">
                    <input type="phone" placeholder="<?php echo Yii::t('common', 'Телефон'); ?>">
                    <textarea placeholder="<?php echo Yii::t('common', 'Сообщение'); ?>"></textarea>
                    <input type="submit" class="contacts-button" value="<?php echo Yii::t('common', 'Отправить сообщение'); ?>">
                </form>
            </div>
        </div>
    </div>		
    <div class="contacts-bg">
    </div>
</section>

