<?php echo '<?xml version="1.0" encoding="utf-8"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php if (!empty($category)): ?>
        <?php if (!empty($category))  ?>
        <?php foreach ($category as $key => $value) : ?>
            <url>
                <loc>http://debiliki.ru/category/<?php echo $value['translit']; ?></loc>
            </url>
        <?php endforeach; ?>
            <url>
                <loc>http://debiliki.ru/games</loc>
            </url>
    <?php endif; ?>
    <?php if (!empty($article)): ?>
        <?php foreach ($article as $key => $value) : ?>
            <url>
                <loc>http://debiliki.ru/<?php echo $value['translit']; ?></loc>
            </url>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (!empty($games)): ?>
        <?php foreach ($games as $key => $value) : ?>
            <url>
                <loc>http://debiliki.ru/game/<?php echo $value['translit']; ?></loc>
            </url>
        <?php endforeach; ?>
    <?php endif; ?>
</urlset> 