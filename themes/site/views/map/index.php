<div class="content">
    <h1>карта сайта</h1>
    <?php if (!empty($data)): ?>
        <h2>Категории:</h2>
        <ul>
            <?php foreach ($data as $key => $value): ?>
                <li><a href="/sitemap/<?php echo $value['id'] ?>"><?php echo $value['title']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <?php if (!empty($games)): ?>
        <h2>Игры:</h2>
        <ul>
            <?php foreach ($games as $key => $value): ?>
                <li><a href="/games/<?php echo $value['translit'] ?>"><?php echo $value['title']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <?php if (!empty($video)): ?>
        <h2>Видео:</h2>
        <ul>
            <?php foreach ($video as $key => $value): ?>
                <li><a href="/<?php echo $value['translit'] ?>"><?php echo $value['title']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>