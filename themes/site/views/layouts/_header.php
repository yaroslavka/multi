<header class="main-header">
    <div class="top-header-row">
        <div class="container">
            <div class="row">			
                <div class="col-lg-1 col-md-4 col-sm-4">
                    <div class="logo-wrapper">
                        <a href="#"><img src="img/logo.png" alt="Moscall"></a>
                    </div>
                </div>
                <div class="col-lg-2 col-lg-offset-7 col-sm-4 col-md-offset-2 col-md-3">
                    <ul class="choose-language">
                        <?php $this->widget('application.components.widgets.LanguageSelector');?>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="header-enter-registration-wrapper">
                        <a href="personal-area.html" class="enter-link"><?php echo Yii::t('common', 'Вход'); ?></a>
                        <a href="#" class="registration-link"><?php echo Yii::t('common', 'Регистрация'); ?></a>							
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header-row">
        <div class="container">
            <a href="#" class="toggle-mnu"><span></span></a>
            <div class="row">		
                <div class="col-md-12 col-lg-3 col-lg-offset-2 col-lg-push-7 col-sm-12">
                    <div class="add-funds-wrapper">
                        <a href="refill.html"><?php echo Yii::t('common', 'Выбрать пакет'); ?></a>
                    </div>		
                </div>
                <div class="col-lg-7  col-lg-pull-3 col-md-12 col-sm-12">
                    <div class="header-menu">
                        <nav class="main-menu">
                            <ul class="choose-language-mobile">
                                 <?php $this->widget('application.components.widgets.LanguageSelector');?>
                            </ul>
                            <ul>
                                <li><a href="#"><?php echo Yii::t('common', 'Главная'); ?></a></li>
                                <li><a href="#"><?php echo Yii::t('common', 'Для бизнеса'); ?></a></li>
                                <li><a href="#sn1"><?php echo Yii::t('common', 'Отзывы'); ?></a></li>
                                <li><a href="#sn2"><?php echo Yii::t('common', 'Контакты'); ?></a></li>
                            </ul>
                            <div class="enter-registration-mobile">
                                <a href="personal-area.html" class="enter-link">Вход</a>
                                <a href="#" class="registration-link">Регистрация</a>		
                            </div>
                        </nav>
                    </div>
                </div>						
            </div>
        </div>
    </div>	
</header>