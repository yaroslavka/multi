<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://' . $_SERVER['HTTP_HOST'] . '/js/all.js', CClientScript::POS_END); ?>
<!doctype html>
<html>
    <head>
        <title>Moscall</title>
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon" />
<!--        <link rel="stylesheet" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/css/style.css" />-->
        <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <link rel="stylesheet" href="css/libs/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="css/libs/animate/animate.css">
        <link rel="stylesheet" href="css/libs/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" href="css/libs/Magnific-Popup-master/dist/magnific-popup.css">
        <link rel="stylesheet" href="css/libs/font-awesome-4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="css/fonts.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/media.css">

        <script src="js/libs/modernizr/modernizr.js"></script>
    </head>
    <body>
        <?php $this->renderPartial('//layouts/_header'); ?>
        <?php echo $content; ?>
        <?php $this->renderPartial('//layouts/_footer'); ?>
    </body>  
</html>






