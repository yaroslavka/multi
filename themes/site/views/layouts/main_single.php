<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<?php Yii::app()->clientScript->registerScriptFile('http://' . $_SERVER['HTTP_HOST'] . '/js/all.js', CClientScript::POS_END); ?>
<!doctype html>
<html>
    <head>
        <title><?php echo!empty($this->meta_title) ? $this->meta_title : 'Debiliki.ru - сайт интересных видео' ?></title>
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon" />
        <meta name="description" content="<?php echo strip_tags($this->meta_description); ?>" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/css/style.css" />
        <meta charset="utf-8">
    </head>
    <body>
        <div class="container">
            <div class="row header">
                <div class="col-md-12">
                    <?php $this->renderPartial('//layouts/_header'); ?>
                </div>
            </div>
            <div class="row page">
                <div class="col-md-3 aside_r">
                    <a class="plg" target="_blank" href="https://play.google.com/store/apps/details?id=com.ionicframework.myapp889549">
                        <img src="../img/plg.jpeg" alt="">
                    </a>
                <h3>Категории:</h3>
                    <?php if (!empty($this->category)): ?>
                        <ul class="nav nav-pills nav-stacked">
                            <?php foreach ($this->category as $key => $value): ?>
                                <li class="special <?php echo in_array($value->id, $this->cat_active) ? 'active' : ''; ?>"><a href="/category/<?php echo $value->translit; ?>"><?php echo $value->title; ?> <span class="badge pull-right"><?php echo $value->count; ?></span></a></li>
                            <?php endforeach; ?>
                            <li class="special <?php echo Yii::app()->controller->action->id=='games'||Yii::app()->controller->action->id=='game'?'active':'';?>"><a href="/games">Игры<span class="badge pull-right"><?php echo $this->games_count; ?></span></a></li>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="col-md-6 conetnt">
                    <?php echo $content; ?>
                </div>
                <div class="col-md-3 aside_l">
                    <?php if (!empty($this->popular)): ?>
                        <h3>Популярное:</h3>
                        <?php foreach ($this->popular as $key => $value): ?>
                            <div class="media">
                                <?php if (!empty($value->image)): ?>
                                    <a class="pull-left" href="/<?php echo $value->translit; ?>">
                                        <img class="media-object" src="<?php echo $value->getImagePath('logo'); ?>" alt="<?php echo $value->title; ?>" />
                                    </a>
                                <?php endif; ?>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="/<?php echo $value->translit; ?>"><?php echo $value->title; ?></a></h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row footer">
                <?php $this->renderPartial('//layouts/_footer'); ?>
            </div>
        </div>
    </body>  
</html>
