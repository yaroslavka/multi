<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <p class="footer-descr">Moscall - приложение, которое позволяет всегда оставаться на связи.
                    Скачайте и установите приложение, чтобы осуществлять дешевые
                    звонки с качественной связью.</p>
                <div class="mobile-buttons">
                    <a href="#"><img src="img/googlee-play-btn.png" alt=""><img src="img/googlee-play-btn1.png" alt=""></a>
                    <a href="#"><img src="img/appstore-btn.png" alt=""><img src="img/appstore-btn1.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <p class="user-conditions"><a href="#">Условия пользования</a></p>
            </div>	
            <div class="col-md-4">
                <ul class="footer-links">
                    <li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                </ul>					
            </div>
            <div class="col-md-4">
                <p class="copyright">Moscall &copy; 2016</p>
            </div>
        </div>
    </div>
</footer>

<div class="hidden"></div>

<div class="loader">
    <div class="loader_inner"></div>
</div>

<!--[if lt IE 9]>
<script src="libs/html5shiv/es5-shim.min.js"></script>
<script src="libs/html5shiv/html5shiv.min.js"></script>
<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
<script src="libs/respond/respond.min.js"></script>
<![endif]-->

<script src="js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="js/libs/plugins-scroll/plugins-scroll.js"></script>
<script src="js/libs/slick-1.6.0/slick/slick.min.js"></script>
<script src="js/libs/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="js/libs/wow-js/wow.min.js"></script>
<script src="js/libs/pageScroll2id/pageScroll2id.min.js"></script>
<script src="js/common.js"></script>