<?php Yii::app()->clientScript->registerPackage('jquery'); ?>
<!doctype html>
<html>
    <head>
        <title><?php echo !empty($this->meta_title)?$this->meta_title:'Debiliki.ru - сайт интересных видео'?></title>
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon" />
        <meta name="description" content="<?php echo strip_tags($this->meta_description); ?>" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/css/style.css" />
        <meta charset="utf-8">
    </head>
    <body>
    <div class="container">
        <div class="row header">
            <div class="col-md-12">
                <?php $this->renderPartial('//layouts/_header');?>
            </div>
        </div>
        <div class="row page">
            <div class="col-md-12 conetnt">
                <?php echo $content; ?>
            </div>
        </div>
        <div class="row footer">
            <?php $this->renderPartial('//layouts/_footer');?>
        </div>
    </div>
</body>  
</html>






