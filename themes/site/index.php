<?php if (!empty($search)): ?>
    <div class="search">Вы искали: <span>"<?php echo $search; ?>"</span></div>
<?php endif; ?>
<?php if (!empty($model)): ?>
    <?php if (!empty($model['left'])): ?>
        <div class="col-md-4">
            <?php foreach ($model['left'] as $key => $value): ?>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <?php if (!empty($value->image)): ?>
                            <a class="imgWrap" href="/<?php echo $value->translit; ?>"><img class="img-responsive" src="<?php echo $value->getImagePath('logo'); ?>" alt="<?php echo $value->title; ?>" /></a>
                        <?php endif; ?>
                        <div class="caption">
                            <h2><a href="/<?php echo $value->translit; ?>"><?php echo $value->title; ?></a></h2>
<!--                            <div class="cold-md-12 retingRow">
                                <i class="glyphicon glyphicon-eye-open">228</i>
                                <i class="glyphicon glyphicon-thumbs-up">12</i>
                                <i class="glyphicon glyphicon-thumbs-down">8</i>
                            </div>-->
                            <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="icon" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus,surfingbird"></div>
                        </div>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($model['center'])): ?>
        <div class="col-md-4">
            <?php foreach ($model['center'] as $key => $value): ?>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <?php if (!empty($value->image)): ?>
                            <a class="imgWrap" href="/<?php echo $value->translit; ?>"><img class="img-responsive" src="<?php echo $value->getImagePath('logo'); ?>" alt="<?php echo $value->title; ?>" /></a>
                        <?php endif; ?>
                        <div class="caption">
                            <h2><a href="/<?php echo $value->translit; ?>"><?php echo $value->title; ?></a></h2>
                            <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="icon" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus,surfingbird"></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($model['right'])): ?>
        <div class="col-md-4">
            <?php $i=0; foreach ($model['right'] as $key => $value): $i++;?>
                <?php if($i==4):?>
                 <?php endif;?>   
                <div class="col-md-12">
                    <div class="thumbnail">
                        <?php if (!empty($value->image)): ?>
                            <a class="imgWrap" href="/<?php echo $value->translit; ?>"><img class="img-responsive" src="<?php echo $value->getImagePath('logo'); ?>" alt="<?php echo $value->title; ?>" /></a>
                        <?php endif; ?>
                        <div class="caption">
                            <h2><a href="/<?php echo $value->translit; ?>"><?php echo $value->title; ?></a></h2>
                            <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="icon" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus,surfingbird"></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php else: ?>
    <?php if (!empty($search)): ?><p class="search_description">По вашему запросу ничего не найдено.</p><?php endif; ?>
<?php endif; ?>
<div class="pagingWrapper">
    <?php if (!empty($pages) && !empty($count)): ?>
        <?php $this->widget('CLinkPager', array('htmlOptions' => array('class' => 'pagination pagination-lg iblock', 'id' => 'pagination'), 'pages' => $pages, 'header' => '', 'firstPageLabel' => '&laquo;', 'lastPageLabel' => '&raquo;', 'nextPageLabel' => '&#8250;', 'prevPageLabel' => '&#8249;')); ?>
    <?php endif; ?>
</div>













