$(function() {
//SCROLL MENU
$(".main-menu a").mPageScroll2id({
	scrollSpeed: 350
});
	//Animation wow
      new WOW().init();

//TABS NAVIGATION
$('.tabs-call-btns li').click(function(e) {
  var a = $(this),
      parent = a.parents('.tabs-call-wrapper'),
      nav = parent.children('.tabs-call-btns').children('a'),
      box = parent.children('.call-tabs').children('tbody');
 
  if (!a.hasClass('active-download')) {
    a.addClass('active-download')
      .siblings().removeClass('active-download');
 
    box.eq(a.index()).addClass('active-download')
      .siblings().removeClass('active-download');
  }
 
  e.preventDefault();
});

// $(window).load(function () {
//         // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
//         $.magnificPopup.open({
//             items: {
//                 src: '#enter-sucсess'
//             },
//             type: 'inline',
//             fixedContentPos: false,
// 						fixedBgPos: true,

// 						overflowY: 'auto',

// 						closeBtnInside: true,
// 						preloader: false,
						
// 						midClick: true,
// 						removalDelay: 300,
// 						mainClass: 'my-mfp-slide-bottom'

//           // You may add options here, they're exactly the same as for $.fn.magnificPopup call
//           // Note that some settings that rely on click event (like disableOn or midClick) will not work here
//         }, 0);
//     });


$('a[href=#enter-1], a[href=#enter-3], a[href=#enter-4]').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

	

$.fn.forceNumbericOnly = function() {
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            return ( key == 8 || key == 9 || key == 46 ||(key >= 37 && key <= 40) ||(key >= 48 && key <= 57) ||(key >= 96 && key <= 105) || key == 107 || key == 109 || key == 173|| key == 61  ); });
    });
};
$('.enter-your-phone').forceNumbericOnly();
$('.enter-phone-popup').forceNumbericOnly();




$(".choose-language").click(function() {
			 
	if($(".change-lang-hover").is(":visible")){
			$(".change-lang-hover").fadeOut(1);
		} else{
			$(".change-lang-hover").fadeIn(1);
		};

	});
$(".choose-language > li > a").click(function(e) {
	e.preventDefault();
});
$(".choose-language-mobile").click(function() {
			 
	if($(".change-lang-hover").is(":visible")){
			$(".change-lang-hover").fadeOut(1);
		} else{
			$(".change-lang-hover").fadeIn(1);
		};

	});
$(".choose-language-mobile > li > a").click(function(e) {
	e.preventDefault();
});

$('.reviews-slider-wrapper').slick({
  slidesToShow: 2,
  dots: true,
  adaptiveHeight: true,
  arrows: false,
  slidesToScroll: 2,
  responsive: [
  {
      breakpoint: 992,
      settings: {
        arrows: false,
        centerMode: false,
        dots: true,
        centerPadding: '40px',
        slidesToShow: 1,
      }
    }
  ]
});

$('.rates-slider-wrapper').slick({
  centerMode: true,
  centerPadding: '0',
  slidesToShow: 3,
  dots: false,
  adaptiveHeight: true,
  nextArrow: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
	prevArrow: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
  responsive: [

  	{
      breakpoint: 992,
      settings: {
        arrows: true,
        centerMode: false,
        dots: true,
        centerPadding: '40px',
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 615,
      settings: {
        arrows: false,
        centerMode: false,
        dots: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
	

	//HEIGHT BLOCKS
	function heightBlock(column){
		var myblock = 0;

		column.each(function(){
			thisHight = $(this).height();
			if(thisHight > myblock){
				myblock = thisHight;
			}
		});
		column.height(myblock);		
	};
	heightBlock($(".reviews-slider"));



	//TOGGLE MENU
	$(".toggle-mnu").click(function() {
		$(this).toggleClass("on");
	});

	$(".toggle-mnu").click(function() {
		if($(".main-menu").is(":visible")){
			$(".main-menu").fadeOut(1);
		} else{
			$(".main-menu").fadeIn(1);
		};
	});
// $(".main-menu ul li a").click(function(){
// 	$(".main-menu").fadeOut(100);
// 	$(".toggle-mnu").removeClass("on");
// })




	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};


	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });
	
});

$(window).load(function() {

	$(".loader_inner").fadeOut();
	$(".loader").delay(400).fadeOut("slow");

});
