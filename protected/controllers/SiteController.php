<?php

class SiteController extends Controller {

    public function actionLogout(){
        Yii::app()->user->logout(false);
        $this->redirect(array('site/login'));
    }

    public function actionLogin() {
        Yii::app()->theme = 'ace';
        $this->layout = '//layouts/column_login';
        $model = new AdminLoginForm;
        if (isset($_POST['AdminLoginForm'])) {
            $model->attributes = $_POST['AdminLoginForm'];
            if ($model->validate() && $model->login()) {
                $this->redirect('/adminUser');
            }
        } $this->render('login', array('model' => $model));
    }
    
    public function actionError()
    {
        $this->layout = '//layouts/main';
        if($error=Yii::app()->errorHandler->error){
            if(Yii::app()->request->isAjaxRequest){
                echo $error['message'];
            } else{
                $this->render('error',$error);
            }
        }
    }

    public function actionIndex() {
        $advantages = Advantage::model()->findAll(array('order'=>'depth ASC'));
        $reviews = Review::model()->findAll(array('order'=>'depth ASC'));
        $settings = Settings::model()->find();
        $contacts = Contacts::model()->find();
        $this->render('index', array('settings'=>$settings,'advantages'=>$advantages,'reviews'=>$reviews,'contacts'=>$contacts));
    }
    
    public function actionBlog() {
        echo Yii::app()->language;
        $this->render('index', array());
    }
    
    public function actionSingle($url) {
        $model = Blog::model()->findByAttributes(array('translit'=>$url));
        $this->render('single', array('model'=>$model));
    }
    
    public function actionStatics($url) {
        $model = Statics::model()->findByAttributes(array('translit'=>$url));
        $this->render('statics', array('model'=>$model));
    }
}
