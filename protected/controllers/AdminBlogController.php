<?php

class AdminBlogController extends AdminController
{

    public $defaultAction='admin';

    public function filters()
    {
        return array(
            'accessControl',// perform access control for CRUD operations
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',// allow admin user to perform 'admin' and 'delete' actions
                'expression'=>'Yii::app()->user->checkAccess("Administrator")',
            ),
            array('deny',// deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new Blog;
        $this->layout='//layouts/column_create';
        if(isset($_POST['Blog'])){
            $model->attributes=$_POST['Blog'];
            if($model->save()) {
                $this->redirect(array('view','id'=>$model->primaryKey));
            }
        }
        $this->render('create',array('model'=>$model));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $this->layout='//layouts/column_create';
        if(isset($_POST['Blog'])){
            $model->attributes=$_POST['Blog'];
            if($model->save()) $this->redirect(array('view','id'=>$model->primaryKey));
        }
        $this->render('update',array('model'=>$model));
    }

    public function actionAdmin()
    {
        $model=new Blog('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Blog'])) $model->attributes=$_GET['Blog'];
        $this->render('admin',array('model'=>$model));
    }
    
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if(!isset($_GET['ajax'])) $this->redirect(isset($_POST['returnUrl'])?$_POST['returnUrl']:array('admin'));
    }

    public function loadModel($id)
    {
        $model=Blog::model()->findByPk($id);
        if($model===null) throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    
    public function actionFileDelete($id,$field)
    {
        try{
            $model=$this->loadModel($id);
            @unlink($model->{$field});
            $model->$field=null;
            $model->save();
            echo 1;
        } catch(Exception $exc){
            echo $exc->getTraceAsString();
        }
    }
    
}
