<?php

class AdminSettingsController extends AdminController
{

    public $defaultAction='admin';

    public function filters()
    {
        return array(
            'accessControl',// perform access control for CRUD operations
        );
    }
    
    public function accessRules()
    {
        return array(
            array('allow',// allow admin user to perform 'admin' and 'delete' actions
                'expression'=>'Yii::app()->user->checkAccess("Administrator")',
            ),
            array('deny',// deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel(1);
        $model->scenario = 'update';
        $this->layout='//layouts/column_create';
        if(isset($_POST['Settings'])){
            $model->attributes=$_POST['Settings'];
            if($model->save()) {
                $this->redirect(array('view','id'=>$model->primaryKey));
            }
        }
        $this->render('update',array('model'=>$model));
    }

    public function actionAdmin()
    {
        $model=$this->loadModel(1);
        if(isset($_GET['Settings'])) $model->attributes=$_GET['Settings'];
            if(isset($_POST['Settings'])){
            $model->attributes=$_POST['Settings'];
            if($model->save()) {
                $this->redirect(array('/adminSettings'));
            }
        }
        $this->render('admin',array('model'=>$model));
    }
    
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if(!isset($_GET['ajax'])) $this->redirect(isset($_POST['returnUrl'])?$_POST['returnUrl']:array('admin'));
    }

    public function loadModel($id)
    {
        $model=Settings::model()->findByPk($id);
        if($model===null) throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
