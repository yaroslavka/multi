<?php

class InfoBehavior extends CActiveRecordBehavior
{
        public $classmodel; // Например NodeInfo
        public $description='description';
        public $title='title';        
        public $metadescription='meta_description';
        public $metatitle='meta_title';        
        public $tid='tid';        
        
        private $_info;
        
        public function getInfo()
        {
                $class=$this->classmodel;
                if($this->getOwner()->isNewRecord)
                        $this->_info=new $class;
                else
                {
                        $this->_info=$class::model()->findByAttributes(array($this->tid=>$this->getOwner()->primaryKey));
                        if(empty($this->_info->primaryKey))
                               $this->_info=new $class; 
                }
                
                return $this->_info;
        }
        
        public function setInfo($value)
        {
                $this->_info=$value;
        }
        
	public function afterSave($event)
	{
                $model=$this->info;
                if(!empty($_POST[$this->classmodel]))
                {
                        $model->attributes=$_POST[$this->classmodel];

                        if(!empty($this->getOwner()->{$this->title})&&empty($model->{$this->metatitle}))
                                $model->{$this->metatitle}=$this->getOwner()->{$this->title};
                        if(!empty($this->getOwner()->{$this->description})&&empty($model->{$this->metadescription}))
                                $model->{$this->metadescription}=Controller::cutString(strip_tags($this->getOwner()->{$this->description}),300);

                        $model->tid=$this->getOwner()->primaryKey;
                        $model->save();
                }
                
                return parent::afterSave($event);
	}
}
