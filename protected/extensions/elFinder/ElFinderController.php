<?php

// controller to host connector action
class ElFinderController extends CController
{
        public function actions()
        {
                return array(
                        'connector'=>array(
                                'class'=>'ext.elFinder.ElFinderConnectorAction',
                                'settings'=>array(
                                        'root'=>Yii::getPathOfAlias('webroot').'/files/',
                                        'URL'=>Yii::app()->baseUrl.'/files/',
                                        'rootAlias'=>'Home',
                                        'mimeDetect'=>'none'
                                )
                        ),
                );
        }
}
