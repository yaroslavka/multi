<?php

/**
 * Версия 2.0. Загрузка и нарезка файлов для нескольких полей в БД
 */
class ImagesBehavior extends CActiveRecordBehavior
{

    public $file_name=array('image'=>'file_image');
    public $versions=array();
    public $file_upload='files';
    public $root_upload='webroot';
    public $file_size=3;
    public $file_qty=10;
    
    /**
     * Расширяем метод после сохранения
     * @param type $event
     * @return type
     */
    public function afterSave($event)
    {
        if(!empty($_FILES)){
            $folder=YiiBase::getPathOfAlias($this->root_upload.'.'.$this->file_upload.'.'.$this->getOwner()->tableName());
            if(!is_dir($folder)){
                mkdir($folder,0755,true);
            }
            foreach($this->file_name as $image=>$file_name){
                // сохраняем файлы
                $fileimage=CUploadedFile::getInstance($this->getOwner(),$file_name);
                if(!empty($fileimage)){
                    $filename=md5($this->getOwner()->primaryKey.microtime()).'.'.$fileimage->getExtensionName();
                    $folder=YiiBase::getPathOfAlias($this->root_upload.'.'.$this->file_upload.'.'.$this->getOwner()->tableName().'.'.$image);
                    if(!is_dir($folder)){
                        mkdir($folder,0755,true);
                    }
                    if(!empty($this->versions)){
                        foreach($this->versions as $key=>$value){
                            $folder=YiiBase::getPathOfAlias($this->root_upload.'.'.$this->file_upload.'.'.$this->getOwner()->tableName().'.'.$image.'.'.$key);
                            if(!is_dir($folder)){
                                mkdir($folder,0755,true);
                            }
                        }
                    }
                    $path=$this->file_upload.DIRECTORY_SEPARATOR.$this->getOwner()->tableName().DIRECTORY_SEPARATOR.$image;
                    if($fileimage->saveAs($path.DIRECTORY_SEPARATOR.$filename)){
                        $this->getOwner()->updateByPk($this->getOwner()->primaryKey,array($image=>$filename));
                        if(!empty($this->versions)){
                            $_image=Yii::app()->image->load($path.DIRECTORY_SEPARATOR.$filename);
                            foreach($this->versions as $key=>$value){
                                $_image->centeredpreview($value['centeredpreview'][0],$value['centeredpreview'][1]);
                                $_image->save($path.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR.$filename);
                            }
                        }
                    }
                }
            }
        }
        return parent::afterSave($event);
    }

    /**
     * метод валидации
     * @param type $owner
     */
    public function attach($owner)
    {
        parent::attach($owner);
        $validators=$this->getOwner()->getValidatorList();
        $params=array(
            'types'=>'jpg,jpeg,png,gif',
            'allowEmpty'=>true,
            'wrongType'=>Yii::t('app','Допустимы только файлы следующих форматов: {extensions}.'),
            'maxSize'=>1024*1024*($this->file_size),
            'tooLarge'=>Yii::t('app','Указан файл объемом более '.$this->file_size.'Мб. Пожалуйста, укажите файл меньшего размера.'),
            'maxFiles'=>$this->file_qty,
        );
        $validator=CValidator::createValidator('file',$this->getOwner(),$this->file_name,$params);
        $validators->add($validator);
    }

    /**
     * Получить путь к фото
     * @param type $field - поле
     * @param type $image - версия нарезки к полю
     * @return string
     */
    public function getImagesReal($field='image',$image=null)
    {
        $path='';
        if(isset($image)){
            if(!empty($this->getOwner()->{$field})) {
                $path=$this->file_upload.DIRECTORY_SEPARATOR.$this->getOwner()->tableName().DIRECTORY_SEPARATOR.$field.DIRECTORY_SEPARATOR.$image.DIRECTORY_SEPARATOR.$this->getOwner()->{$field};
            }
        } else {
            if(!empty($this->getOwner()->{$field})) {
                $path=$this->file_upload.DIRECTORY_SEPARATOR.$this->getOwner()->tableName().DIRECTORY_SEPARATOR.$field.DIRECTORY_SEPARATOR.$this->getOwner()->{$field};
            }
        }
        return $path;
    }

    /**
     * Перед сохранением
     * @param type $event
     * @return type
     */
    public function beforeSave($event)
    {
        if(!empty($_FILES)){
            foreach($this->file_name as $image=>$file_name){
                $fileimage=CUploadedFile::getInstance($this->getOwner(),$file_name);
                if(!empty($fileimage)){
                    $this->deleteImages($image);
                }
            }
        }
        return parent::beforeSave($event);
    }
    
    /**
     * Перед удалением
     * @param type $event
     * @return type
     */
    public function beforeDelete($event)
    {
        $this->deleteImages();
        return parent::beforeDelete($event);
    }
    
    /**
     * Управление удаление фото или нарезками
     * @param type $node
     */
    private function deleteImages($node=null) 
    {
        if(empty($node)){
            foreach($this->file_name as $image=>$file_name){
                $this->deleteSingle($image);
            }            
        } else {
            $this->deleteSingle($node);
        }
    }
    
    /**
     * Удаление фото и нарезок
     * @param type $node
     */
    private function deleteSingle($node)
    {
        if(!empty($this->versions)){
            $name=$this->getOwner()->{$node};
            $path=$this->file_upload.DIRECTORY_SEPARATOR.$this->getOwner()->tableName().DIRECTORY_SEPARATOR.$node;
            if(file_exists($path.DIRECTORY_SEPARATOR.$name)){
                @unlink($path.DIRECTORY_SEPARATOR.$name);
            }
            foreach($this->versions as $key=>$value){
                if(file_exists($path.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR.$name)){
                    @unlink($path.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR.$name);
                }
            }
        }         
    }

}
