<?php

class UploadBehavior extends CActiveRecordBehavior
{

    public $file_name='file_upload';
    public $name='upload';
    public $file_size=25;
    public $versions=array();
    public $file_upload='files';
    public $root_upload='webroot';
    private $_file_upload;

    public function getFile_upload()
    {
        return $this->_file_upload;
    }
    
    public function afterSave($event)
    {
        if(!empty($_FILES)){
            // сохраняем файл
            $fileimage=CUploadedFile::getInstance($this->getOwner(),$this->file_name);
            if(!empty($fileimage)){
                if(!empty($this->getOwner()->translit)) $filename=$this->getOwner()->translit.'.'.$fileimage->getExtensionName();
                else $filename=md5($this->getOwner()->primaryKey.microtime()).'.'.$fileimage->getExtensionName();
                $folder=YiiBase::getPathOfAlias($this->root_upload.'.'.$this->file_upload.'.'.$this->getOwner()->tableName());
                if(!is_dir($folder)) mkdir($folder,0755,true);
                $path=$this->file_upload.DIRECTORY_SEPARATOR.$this->getOwner()->tableName();
                if($fileimage->saveAs($path.DIRECTORY_SEPARATOR.$filename)){
                    $this->getOwner()->updateByPk($this->getOwner()->primaryKey,array($this->name=>$filename));
                }
            }
        }
        return parent::afterSave($event);
    }

    public function attach($owner)
    {
        parent::attach($owner);
        $validators=$this->getOwner()->getValidatorList();
        $params=array(
            'types'=>'mpg,avi,flv,mpeg,mp4',
            'allowEmpty'=>true,
            'wrongType'=>Yii::t('app','Допустимы только файлы следующих форматов: {extensions}.'),
            'maxSize'=>1024*1024*($this->file_size),
            'tooLarge'=>Yii::t('app','Указан файл объемом более '.$this->file_size.'Мб. Пожалуйста, укажите файл меньшего размера.'),
            'maxFiles'=>1,
        );
        $validator=CValidator::createValidator('file',$this->getOwner(),$this->file_name,$params);
        $validators->add($validator);
    }

    public function getImageReal($field='image')
    {
        $path='';
        if(!empty($this->getOwner()->{$field})) {
            $path=$this->file_upload.DIRECTORY_SEPARATOR.$this->getOwner()->tableName().DIRECTORY_SEPARATOR.$this->getOwner()->{$field};
        }
        return $path;
    }

}