<?php

class DefaultController extends Controller
{
	/**
	 * Displays sitemap in XML or HTML format,
	 * depending on the value of $format parameter
	 * @param string $format
	 */
	public function actionIndex($format = 'xml')
	{
		if ($this->getModule()->actions)
			$urls = $this->getModule()->getSpecifiedUrls();
		else
			$urls = $this->getModule()->getAllUrls();
			
		if ($format == 'xml')
		{
			if (!headers_sent())
				header('Content-Type: text/xml');
			$this->renderPartial('xml', array('urls' => $urls));
		}
		else
			$this->render('html', array('urls' => $urls));
	}
	
	public function actionMaptxt()
	{
		if ($this->getModule()->actions)
			$urls = $this->getModule()->getSpecifiedUrls();
		else
			$urls = $this->getModule()->getAllUrls();

                $l='';
                foreach ($urls as $url => $data)
                        $l.=$url.'; '.iconv("utf-8", "windows-1251",(!empty($data['headertitle'])?$data['headertitle']:''))."\r\n";
                
                header('Content-Type: text/plain');
                
                file_put_contents(Yii::getPathOfAlias('webroot').'/sitemap.txt',$l,LOCK_EX);                
                
                $this->renderPartial('txt', array('urls' => $urls));
	}   	
}