<?php if(!empty($model)):?>
    <div class="outside_rec row">
        <div class="title row aleft">
            РЕКЛАМА
        </div>
        <div class="row">
            <?php foreach ($model as $key => $value):?>
            <div class="item ibleft relative">
                <a href="<?php echo Yii::app()->createUrl('/site/redirectAdv',array('url'=>$value->url));?>">
                    <?php if (file_exists(Yii::getPathOfAlias('webroot') . '/' . $value->getImageReal('image', 'logo'))): ?>
                        <?php echo CHtml::image($value->getImageReal('image', 'logo')); ?>
                    <?php endif; ?>
                    <div class="descr aleft">
                        <?=!empty($value->title)?$value->title:'';?>
                    </div>
                </a>
            </div>
            <?php endforeach;?>
        </div>
        <div class="row">
            <a href="<?php echo Yii::app()->createUrl('/site/advertisementPage');?>" class="see_all ibright">СМОТРЕТЬ ВСЕ</a>
        </div>
    </div>
<?php endif;?>