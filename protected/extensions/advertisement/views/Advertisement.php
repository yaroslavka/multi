<div class="inside_rec row">
    <a href="<?php echo Yii::app()->createUrl('/site/redirectAdv', array('url' => $model->url)); ?>">
        <div class="row">
            <?php if (file_exists(Yii::getPathOfAlias('webroot') . '/' . $model->getImageReal('image', 'quart'))): ?>
                <?php echo CHtml::image($model->getImageReal('image', 'quart')); ?>
            <?php endif; ?>
        </div>
        <div class="title row aleft">
            <?= !empty($model->title) ? $model->title : ''; ?>
        </div>
    </a>
        <div class="row mgt10 aleft">
            <?= !empty($model->description) ? $model->description : ''; ?>
        </div>
    
    <div class="row">
        <a href="<?php echo Yii::app()->createUrl('/site/advertisementPage'); ?>" class="see_all ibright">СМОТРЕТЬ ВСЕ</a>
    </div>
</div>