<?php 

class Advertisement extends CWidget{
    
    public $quantity;
    
    public function run() {
        
        if($this->quantity == 'one'){
            $model= AdminAdvertising::model()->find('status=2');
        }else if($this->quantity=='few'){
            $criteria = new CDbCriteria();
            $criteria->limit = 4;
            $criteria->condition='status=1';
            $model= AdminAdvertising::model()->findAll($criteria);
        }
        
        if(!empty($model) && $this->quantity=='one'){
            $this->render('Advertisement',array('model'=>$model));
        }else if(!empty($model) && $this->quantity=='few'){
             $this->render('Few_advertisement',array('model'=>$model));
        }    
    }
   
    
}
?>