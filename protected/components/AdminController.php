<?php

class AdminController extends CController
{

    public $layout='//layouts/column';
    
    public function init()
    {
        return parent::init();
    }
    
    public static function generate($n=5,$pattern=1)
    {
        $key='';
        $_pattern='123456789';
        if($pattern==2)
            $_pattern='0123456789abcdefghijgklmnopqrstuvwxyz';
        elseif($pattern==3)
            $_pattern='0123456789abcdef';
        $counter=strlen($_pattern)-1;
        for($i=0; $i<$n; $i++)
            $key.=$_pattern{rand(0,$counter)};
        return $key;
    }

    public static function cutString($string,$maxlen)
    {
        $len=(mb_strlen($string)>$maxlen)?mb_strripos(mb_substr($string,0,$maxlen),' '):$maxlen;
        $cutStr=mb_substr($string,0,$len);
        return (mb_strlen($string)>$maxlen)?''.$cutStr.'...':''.$cutStr.'';
    }
    public function beforeAction($action) 
    {
       return parent::beforeAction($action);
    }
    
}
