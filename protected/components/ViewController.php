<?php

class ViewController extends CController
{
    public $breadcrumbs=array(); 
    public $category=array();
    public $games_count=array();
    public $cat_active = array();
    public $popular = array();
    public $cat_id;
    public $meta_title;
    public $meta_description;
    public $canonical;
    
    public $h1;

    public function init()
    {
        Yii::app()->theme = 'site';
        return parent::init();
    }
    
    
    protected function initAlias()
    {
        $_url=Yii::app()->request->requestUri;
        if(!empty($_GET['page'])){
            $_path=Yii::app()->request->requestUri;
            $path=explode("/page/",$_path);
            $_url=!empty($path[0])?$path[0]:'/';
        }
       // $model = AdminAlias::model()->findByAttributes(array('url'=>$_url));
        if(!empty($model)){
            $this->meta_title =!empty($model->meta_title)?$model->meta_title:'';
            $this->meta_description =!empty($model->meta_description)?$model->meta_description:'';
            $this->h1 =!empty($model->h1)?$model->h1:'';
        }
        if(!empty($_GET['page'])){
            $this->meta_title =!empty($model->meta_title)?'Страница №'.$_GET['page'].' &mdash; '.$model->meta_title:'';
            $this->canonical = $_url;
        }
    }
    
    protected function mailto($_subject,$_message,$mail)
    {
        $subject='=?utf-8?B?'.base64_encode($_subject).'?=';
        $message=iconv("utf-8","koi8-r//TRANSLIT",$_message);
        $header='';
        $header.='MIME-Version: 1.0'."\r\n";
        $header.="Content-type: text/html; charset=koi8-r"."\r\n";
        $header.="Date: ".date("r")."\r\n";
        $header.="From: ".Yii::app()->params['adminEmail']."\r\n";
        $header.="To: ".$mail."\r\n";
        $header.="Subject: =?utf-8?B?".base64_encode($_subject)."?="."\r\n";
        $header.="Content-type: text/html; charset=koi8-r"."\r\n";
        return mail($mail,$subject,$message,$header);
    }
    
        public static function generate($n=5,$pattern=1)
    {
        $key='';
        $_pattern='123456789';
        if($pattern==2) $_pattern='0123456789abcdefghijgklmnopqrstuvwxyz';
        elseif($pattern==3) $_pattern='0123456789abcdef';
        $counter=strlen($_pattern)-1;
        for($i=0; $i<$n; $i++) $key.=$_pattern{rand(0,$counter)};
        return $key;
    }

    /**
     * Нормальная обрезка текста
     * @param type $string
     * @param type $maxlen
     * @return type 
     */
    public function cutString($string,$maxlen)
    {
        $len=(mb_strlen($string)>$maxlen)?mb_strripos(mb_substr($string,0,$maxlen),' '):$maxlen;
        $cutStr=mb_substr($string,0,$len);
        return (mb_strlen($string)>$maxlen)?''.$cutStr.' ...':''.$cutStr.'';
    }

    static function mb_ucfirst($str,$encoding="UTF-8",$lower_str_end=false)
    {
        $first_letter=mb_strtoupper(mb_substr($str,0,1,$encoding),$encoding);
        $str_end="";
        if($lower_str_end){
            $str_end=mb_strtolower(mb_substr($str,1,mb_strlen($str,$encoding),$encoding),$encoding);
        } else{
            $str_end=mb_substr($str,1,mb_strlen($str,$encoding),$encoding);
        }
        $str=$first_letter.$str_end;
        return $str;
    }
    
    static function normalDate($time)
    {
        $date = '';
        if(!empty($time)){
           $months = array('01'=>'января','02'=>'февраля','03'=>'марта','04'=>'апреля','05'=>'мая','06'=>'июня','07'=>'июля','08'=>'августа','09'=>'сентября','10'=>'октября','11'=>'ноября','12'=>'декабря');
           $day = strftime("%d",strtotime($time));
           $month = strftime("%m",strtotime($time));
           $year = strftime("%Y",strtotime($time));
           $date = $day.' '.$months[$month].' '.$year;
        }
        return $date;
    }
    

    /**
     * Времени назад
     * @param type $tm
     * @param type $rcs
     * @return type 
     */
    public function ago($tm,$rcs=0)
    {
        $cur_tm=time();
        $dif=$cur_tm-$tm;
        $pds=array(Yii::t('app','second'),Yii::t('app','minute'),Yii::t('app','hour'),Yii::t('app','day'),Yii::t('app','week'),Yii::t('app','month'),Yii::t('app','year'),Yii::t('app','decade'));
        $lngh=array(1,60,3600,86400,604800,2630880,31570560,315705600);
        for($v=sizeof($lngh)-1; ($v>=0)&&(($no=$dif/$lngh[$v])<=1); $v--) ;
        if($v<0) $v=0;
        $_tm=$cur_tm-($dif%$lngh[$v]);
        $no=floor($no);
        if($no<>1) $pds[$v].='s';
        $x=sprintf("%d %s ",$no,$pds[$v]);
        if(($rcs==1)&&($v>=1)&&(($cur_tm-$_tm)>0)) $x.= time_ago($_tm);
        return $x;
    }

    public function month_trans($value)
    {
        $a=array('January','February','March','April','May','June','July','August','September','October','November','December');
        $b=array('января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');
        return str_replace($a,$b,$value);
    }

    public function month_contraction($value)
    {
        $a=array('January','February','March','April','May','June','July','August','September','October','November','December');
        $b=array('ЯНВ','ФЕВ','МРТ','АПР','МАЙ','ИЮН','ИЮЛ','АВГ','СЕН','ОКТ','НБР','ДЕК');
        return str_replace($a,$b,$value);
    }

    public function browser_info($agent=null)
    {
        $known=array('msie','firefox','safari','webkit','opera','netscape','konqueror','gecko');
        $agent=strtolower($agent?$agent:$_SERVER['HTTP_USER_AGENT']);
        $pattern='#(?<browser>'.join('|',$known).')[/ ]+(?<version>[0-9]+(?:\.[0-9]+)?)#';
        if(!preg_match_all($pattern,$agent,$matches)){
            return array();
        }
        $i=count($matches['browser'])-1;
        return array($matches['browser'][$i]=>$matches['version'][$i]);
    }

}
