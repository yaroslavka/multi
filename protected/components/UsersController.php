<?php

class UsersController extends CController
{
    public $layout='//layouts/main';
    public $user;
    
    public function init()
    {
        $this->user=UsersRegistration::model()->findByPk(Yii::app()->user->id);
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.selectbox.js');
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/steps.js');
        Yii::app()->theme='site';
        return parent::init();
        
    }
}
