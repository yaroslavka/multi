<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;

    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        
        $user=Users::model()->find('login=:login',array(':login'=>strtolower($this->username)));
        if($user===null) {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        } elseif($user->password!=$this->password) {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id=$user->id;
            /*$lastvisit=date("Y-m-d H:i:s");
            AdminApiUser::model()->updateByPk($this->_id,array('lastvisit'=>$lastvisit));*/
            $this->setState('_obj',$user);
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }

}
