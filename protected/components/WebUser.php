<?php

class WebUser extends CWebUser
{

    private $_model;

    public function getUserData()
    {
        $user=$this->loadUser(Yii::app()->user->id);
        return $user;
    }

    private function loadUser($id=null)
    {
        if($this->_model===null) if($id!==null) $this->_model=User::model()->findByPk($id);
        return $this->_model;
    }

}