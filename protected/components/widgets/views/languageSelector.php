<li>
    <?php echo CHtml::link($currentLang, $this->getOwner()->createMultilanguageReturnUrl($currentLang));?>
    <ul class="change-lang-hover">
        <?php foreach ($languages as $key=>$lang):?>
            <li>
                <?php echo CHtml::link($lang, $this->getOwner()->createMultilanguageReturnUrl($key));?>
            </li>
        <?php endforeach;?>
    </ul>
</li>
