<?php 
return [
    'Преймущества'=>'Advantages',
    'Вход'=>'Sign In',
    'Регистрация'=>'Registration',
    'Выбрать пакет'=>'Choose Rate',
    'Главная'=>'Main',
    'Для бизнеса'=>'For Business',
    'Отзывы'=>'Reviews',
    'Контакты'=>'Contacts',
    'Отзывы Клиентов'=>'Customer Reviews',
    'Контакты'=>'Contacts',
    'Имя'=>'Name',
    'Телефон'=>'Phone',
    'Сообщение'=>'Message',
    'Отправить сообщение'=>'Send message',
    'Тарифы'=>'Rates',
];
?>