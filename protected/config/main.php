<?php

return array(
    'name' => 'moscall',
    'language' => 'ru',
    'sourceLanguage' => 'ru',
    'charset' => 'utf-8',
    'preload' => array(
       // 'log',
    ),
    'theme' => 'ace',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.galleryManager.models.*',
        'ext.galleryManager.*',
        'ext.select2.Select2',
        'ext.CJuiDateTimePicker.CJuiDateTimePicker'
    ),
    'defaultController' => 'site',
    'controllerMap' => array(
        'gallery' => array(
            'class' => 'ext.galleryManager.GalleryController',
        ),
        'tinyMce' => array(
            'class' => 'ext.tinymce.TinyMceController',
        ),
    ),
    'components' => array(
//        'clientScript' => array(
//            'packages' => array(
//                'jquery' => array(
//                    'baseUrl' => '//ajax.googleapis.com/ajax/libs/jquery/1.8.3/',
//                    'js' => array(
//                        'jquery.min.js',
//                    ),
//                )
//            ),
//        ),
        'bootstrap' => (strstr($_SERVER['REQUEST_URI'], 'site') || !isset($_GET['r'])) ? '' : array(
            'class' => 'ext.bootstrap.components.Bootstrap',
                ),
        'iwi' => array(
            'class' => 'application.extensions.iwi.IwiComponent',
            'driver' => 'GD',
        ),
        'image' => array(
            'class' => 'application.extensions.image.CImageComponent',
            'driver' => 'GD',
        ),
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'loginUrl' => array('/site/login'),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=127.0.0.1;dbname=moscall',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'tablePrefix' => '',
            'enableProfiling' => true,
            'enableParamLogging' => true,
            'class'=>'CDbConnection',
        ),
        /* 'cache'=>array(
          'class'=>'system.caching.CMemCache',
          'servers'=>array(
          array('host'=>'localhost','port'=>11211),
          ),
          ), */
        'errorHandler' => array(
            'errorAction' => 'site/error',
            'discardOutput' => false,
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'auth_item',
            'itemChildTable' => 'auth_item_child',
            'assignmentTable' => 'auth_assignment',
            'defaultRoles' => array('Guest')
        ),
        'urlManager'=>array(
            'class'=>'application.components.UrlManager',
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<language:(ru|fr|en)>/' => 'site/index',
                '<language:(ru|fr|en)>/error' => 'site/error',
//                '<language:(ru|ua|en)>/<action:(contact|login|logout)>/*' => 'site/<action>',
//                '<language:(ru|ua|en)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
//                '<language:(ru|ua|en)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
//                '<language:(ru|ua|en)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
                '<language:(ru|ua|en)>/<action:(contact|login|logout)>/*' => 'site/<action>',
                '/adminUser' => 'adminUser',
                '/adminSettings' => 'adminSettings',
                '/adminAdvantage' => 'adminAdvantage',
                '/adminBlog' => 'adminBlog',
                '/adminStatics' => 'adminStatics',
                '/adminReview' => 'adminReview',
                '/adminContacts' => 'adminContacts',
                '<language:(ru|en)>/blog' => 'site/blog',
                '<language:(ru|en)>/<url:[\w\-]+>' => 'site/single',
                '<language:(ru|en)>/statics/<url:[\w\-]+>' => 'site/statics',
               
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CWebLogRoute',
                ),
                array(
                    'class' => 'CProfileLogRoute',
                    'levels' => 'profile',
                    'enabled' => true,
                    //'showInFireBug' => true,
                    'categories' => 'system.db.CDbCommand.query'
                ),
            ),
        ),
    ),
    'params' => array(
        'languages'=>array('ru'=>'RU','en'=>'ENG'),
    ),
);
