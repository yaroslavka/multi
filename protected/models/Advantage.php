<?php

class Advantage extends CActiveRecord
{
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        @unlink('/files/'.$this->image);
        @unlink('/files/mini/'.$this->image);
        @unlink('/files/small/'.$this->image);
        @unlink('/files/logo/'.$this->image);
    }

    public function tableName()
    {
        return 'advantage';
    }
    
    public function rules()
    {
        return array(
            array('title_ru, title_en', 'required'),
            array('descr_ru, descr_en', 'required'),
            array('id, depth','numerical','integerOnly' => true),
            array('id, depth, image','safe'),
        );
    }
    
    public function getTitle()
    {
        $attribute = 'title_' . Yii::app()->getLanguage();
        return $this->{$attribute};
    }
 
    public function setTitle($value)
    {
        $attribute = 'title_' . Yii::app()->getLanguage();
        $this->{$attribute} = $value;
    }
 
    public function getDescr()
    {
        $attribute = 'descr_' . Yii::app()->getLanguage();
        return $this->{$attribute};
    }
 
    public function setDescr($value)
    {
        $attribute = 'descr_' . Yii::app()->getLanguage();
        $this->{$attribute} = $value;
    }
    
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'title_ru' => 'Заголовок (RU)',
            'title_en' => 'Заголовок (ENG)',
            'descr_ru' => 'Описание (RU)',
            'descr_en' => 'Описание (ENG)',
            'depth' => 'Порядок',
            'image' => 'Картинка',
        );
    }
    
    public function behaviors()
    {
        return array(
            'ImageBehavior' => array(
                'class' => 'ext.sprutlab.image.ImageBehavior',
                'file_name' => 'file_image',
                'name' => 'image',
                'versions' => array(
                    'logo' => array(
                        'centeredpreview' => array(250, 250),
                    ),
                ),
            ),
        );
    }
    
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.title_ru', $this->title_ru,true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    '*',
                ),
                'defaultOrder' => 't.id DESC',
            ),
            'pagination' => array(
                'pageSize' => 20,
                'pageVar' => 'page'
            ),
        ));
    }
}
