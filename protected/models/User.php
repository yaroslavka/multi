<?php

class User extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName(){
        return 'user';
    }

    public $hashpassword;
    
    protected function beforeSave() {
        $this->password = CPasswordHelper::hashPassword($this->hashpassword);
        return parent::beforeSave();
    }

    public function rules() {
        return array(
            array('login, hashpassword', 'required'),
            array('hashpassword', 'safe'),
        );
    }
   
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'hashpassword' => 'Пароль',
        );
    }
}
