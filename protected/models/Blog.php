<?php

class Blog extends CActiveRecord {

    public $title_prfx = 'title_';
    public $body_prfx = 'body_';

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function afterDelete() {
        parent::afterDelete();
        @unlink('/files/' . $this->image);
        @unlink('/files/mini/' . $this->image);
        @unlink('/files/small/' . $this->image);
        @unlink('/files/logo/' . $this->image);
    }

    public function tableName() {
        return 'blog';
    }

    public function rules() {
        return array(
            array('title_ru, title_en ,title_fr', 'required'),
            array('body_ru, body_en ,body_fr', 'required'),
            array('id', 'numerical', 'integerOnly' => true),
            array('id, translit, image', 'safe'),
        );
    }

    public function getTitle() {
        $attribute = $this->title_prfx . Yii::app()->getLanguage();
        return $this->{$attribute};
    }

    public function setTitle($value) {
        $attribute = $this->title_prfx . Yii::app()->getLanguage();
        $this->{$attribute} = $value;
    }

    public function getBody() {
        $attribute = $this->body_prfx . Yii::app()->getLanguage();
        return $this->{$attribute};
    }

    public function setBody($value) {
        $attribute = $this->body_prfx . Yii::app()->getLanguage();
        $this->{$attribute} = $value;
    }

    public function getCutBody() {
        $attribute = $this->body_prfx . Yii::app()->getLanguage();
        return AdminController::cutString($this->{$attribute},15);
    }

    public function setCutBody($value) {
        $attribute = $this->body_prfx . Yii::app()->getLanguage();
        $this->{$attribute} = AdminController::cutString($value, 15);
    }

    public function attributeLabels() {
        return array(
            'id' => 'id',
            'title_ru' => 'Заголовок (RU)',
            'title_en' => 'Заголовок (ENG)',
            'title_fr' => 'Заголовок (FR)',
            'body_ru' => 'Содержание (RU)',
            'body_en' => 'Содержание (ENG)',
            'body_fr' => 'Содержание (FR)',
            'translit' => 'Транслит',
            'image' => 'Картинка',
        );
    }

    public function behaviors() {
        return array(
            'ImageBehavior' => array(
                'class' => 'ext.sprutlab.image.ImageBehavior',
                'file_name' => 'file_image',
                'name' => 'image',
                'versions' => array(
                    'mini' => array(
                        'centeredpreview' => array(25, 25),
                    ),
                    'small' => array(
                        'centeredpreview' => array(50, 50),
                    ),
                    'logo' => array(
                        'centeredpreview' => array(250, 250),
                    ),
                ),
            ),
            'TranslitBehavior' => array(
                'class' => 'ext.sprutlab.translit.TranslitBehavior',
                'name' => 'title',
                'model' => __CLASS__,
                'translit' => 'translit',
            ),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.title_ru', $this->title_ru, true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    '*',
                ),
                'defaultOrder' => 't.id DESC',
            ),
            'pagination' => array(
                'pageSize' => 20,
                'pageVar' => 'page'
            ),
        ));
    }

}
