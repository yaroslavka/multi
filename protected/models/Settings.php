<?php

class Settings extends CActiveRecord
{
    public $title_prfx = 'title_';
    public $body_prfx = 'body_';
    
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'settings';
    }
    
    public function rules()
    {
        return array(
            array('top_title_ru, top_title_en', 'required'),
            array('top_descr_ru, top_descr_en', 'required'),
            array('bottom_descr_ru, bottom_descr_en', 'required'),
            array('rate_descr_ru, rate_descr_en', 'required'),
            array('form_descr_ru, form_descr_en', 'required'),
            array('image, link_vk , link_fb, link_android, link_ios, id', 'safe'),
           
        );
    }
    
    
    public function behaviors()
    {
        return array(
            'ImageBehavior' => array(
                'class' => 'ext.sprutlab.image.ImageBehavior',
                'file_name' => 'file_image',
                'name' => 'image',
                'versions' => array(
                    'logo' => array(
                        'centeredpreview' => array(50, 50),
                    ),
                ),
            ),
        );
    }

    public function getAttributeByField($value) {
        $attribute = $value . Yii::app()->getLanguage();
        return $this->{$attribute};
    }
    
    public function setAttributeByField($value)
    {
        $attribute = $value . Yii::app()->getLanguage();
        $this->{$attribute} = $value;
    }
    
    
    
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'top_title_ru' => 'Заголовок Верхний (RU)',
            'top_title_en' => 'Заголовок Верхний (ENG)',
            'top_descr_ru' => 'Текст Верхний (RU)',
            'top_descr_en' => 'Текст Верхний (ENG)',
            'bottom_descr_ru' => 'Текст Нижний (RU)',
            'bottom_descr_en' => 'Текст Нижний (EN)',
            'form_descr_ru' => 'Текст над формой вопроса (RU)',
            'form_descr_en' => 'Текст над формой вопроса (EN)',
            'rate_descr_ru' => 'Текст для тарифов (RU)',
            'rate_descr_en' => 'Текст для тарифов (EN)',
            'image' => 'Логотип',
            'link_vk' => 'Ссылка на VK',
            'link_android' => 'Ссылка на PlayMarket',
            'link_fb' =>'Ссылка на FB',
            'link_ios' =>'Cсылка на IOS',
        );
    }
}
