<?php

class Contacts extends CActiveRecord
{
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'contacts';
    }
    
    public function rules()
    {
        return array(
            array('address_ru, address_en', 'required'),
            array('skype, email , phone, id', 'safe'),
           
        );
    }
    
    public function getAddress()
    {
        $attribute = 'address_' . Yii::app()->getLanguage();
        return $this->{$attribute};
    }
 
    public function setAddress($value)
    {
        $attribute = 'address_' . Yii::app()->getLanguage();
        $this->{$attribute} = $value;
    }
   
    
    
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'address_ru' => 'Адресс (RU)',
            'address_en' => 'Адресс (ENG)',
            'phone' => 'Телефон',
            'email' => 'Email',
            'skype' =>'Скайп',
        );
    }
}
